This repo is all about testing each and every git command that exists.

Enough said, let's get testing!

***

**Some Links**

- Git commit messages:

> http://tbaggery.com/2008/04/19/a-note-about-git-commit-messages.html

- On Git Branching: 

> http://nvie.com/posts/a-successful-git-branching-model/

- You might wanna check your "github resume" 

> http://resume.github.io/

Note: https://blog.jcoglan.com/2013/11/15/why-github-is-not-your-cv/